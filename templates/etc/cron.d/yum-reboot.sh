#!/usr/bin/env bash
# {{ ansible_managed }}

needs_restarting=/usr/bin/needs-restarting

if $needs_restarting -r >/dev/null; then
  #logger -t "$0" 'Cron job will not restart system'
  exit 0
else
  logger -t "$0" 'Cron job will now attempt to restart system'
  if test -f /var/run/yum.pid; then
    logger -t "$0" '/var/run/yum.pid exists, exiting'
  else
    shutdown -r +5
  fi
fi
