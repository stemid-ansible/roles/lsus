# LSUS

Like wsus but for Linux.

Works for:

  * Debian/Ubuntu (unattended-upgrades)
  * RHEL/CentOS (yum-cron)
  * Fedora (dnf-automatic)

See defaults/main.yml for values to set.

For RedHat/CentOS you can set a complicated schedule dictionary that determines what yum-cron does and when.

# TODO

Include null mailer for more distros than just Fedora. Perhaps CentOS minimal installs also lack postfix.
